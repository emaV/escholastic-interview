<?php

/**
 * Test that the block displays the proper fields.
 *
 * Fields which should display:
 * 
 * 1. Block title
 * 2. Description (Trimmed)
 * 3. ISBN-13
 */
